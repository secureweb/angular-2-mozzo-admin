import { MozzoAdminPage } from './app.po';

describe('mozzo-admin App', () => {
  let page: MozzoAdminPage;

  beforeEach(() => {
    page = new MozzoAdminPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
