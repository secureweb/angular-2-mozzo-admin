import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../common/shared.module';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    AdminRoutingModule
  ],
  declarations: [DashboardComponent, AdminComponent]
})
export class AdminModule { 
  constructor() {
    console.log('Admin');
  }
}
