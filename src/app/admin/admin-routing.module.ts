import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin.component';
import { AuthGuard } from './../users/auth-guard.guard';
import { ListComponent } from './../users/list/list.component';
import { CreateComponent } from './../users/create/create.component';
import { UsersComponent } from './../users/users.component';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                canActivateChild: [AuthGuard],
                children: [
                    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
                    { path: 'dashboard', component: DashboardComponent }
                ]
            },
            {
                path: 'messages',
                loadChildren: 'app/messages/messages.module#MessagesModule',
                canLoad: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
    constructor() {
        console.log('AdminRoutingModule');
    }
}
