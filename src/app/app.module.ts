import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { httpFactory } from "./common/http.factory";

import { AppRoutingModule } from './app.routing.module';
import { MaterialSharedModule } from './common/material/material-shared.module';
import { UsersModule } from './users/users.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { LoaderComponent } from './common/loader/loader.component';
import { LoaderService } from './common/loader/loader.service';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoaderComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialSharedModule.forRoot(),
    HttpModule,
    UsersModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    },
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
