import { Component, OnInit, AfterContentInit } from '@angular/core';
import { AuthService } from '../../users/auth.service';
import { UsersModel } from '../../users/users.model';
import { Router } from '@angular/router' 

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterContentInit {
  
  userModel:any;
  constructor(
      public authService: AuthService,
      public router: Router
  ) { }

  private _updateModel(): void {
    this.userModel = this.authService.loggedInUser;
  }

  ngOnInit() {
    this._updateModel();
  }
  
  ngAfterContentInit() {
    this._updateModel();
  }

  logout() {
     this.authService.doLogout().subscribe(( response ) => {
       if(response.success) {
          this.router.navigate(['users/login']);
       }
    });
  }

}
