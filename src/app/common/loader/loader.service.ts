import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoaderService {
    // Observable string sources
    private loaderSource = new Subject<boolean>();

    // Observable string streams
    loaderObs$ = this.loaderSource.asObservable();

    // Service message commands
    showLoader() {
        this.loaderSource.next(true);
    }

    hideLoader() {
        this.loaderSource.next(false);
    }
}