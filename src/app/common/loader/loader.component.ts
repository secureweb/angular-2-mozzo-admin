import { Component } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
 selector: 'app-loader',
 templateUrl: './loader.component.html'
})
export class LoaderComponent {
 loading: boolean = true;

 constructor(loaderService: LoaderService) {
    loaderService.loaderObs$.subscribe((value: boolean) => {
        this.loading = value;
    });
 }
}