//HTTP INTERCEPTOR FOR ADDING CUSTOM Headers

import { Injectable } from "@angular/core";
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers} from "@angular/http";
import { Observable} from "rxjs/Rx";
import { CONFIG } from './configuration';

@Injectable()
export class InterceptedHttp extends Http {
    constructor( 
        backend: ConnectionBackend, 
        defaultOptions: RequestOptions
    ) { super(backend, defaultOptions); }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.getEndpointUrl(url);
        return super.get(url, this.getRequestOptionArgs(options));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.getEndpointUrl(url);
        return super.post(url, body, this.getRequestOptionArgs(options));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.getEndpointUrl(url);
        return super.put(url, body, this.getRequestOptionArgs(options));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.getEndpointUrl(url);
        return super.delete(url, this.getRequestOptionArgs(options));
    }
    
    private getEndpointUrl(req: string) {
        return  CONFIG.endpoint + req;
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.headers.append('Content-Type', 'application/json');
        this.setAuthToken( options );
        return options;
    }

    private setAuthToken( options?: RequestOptionsArgs ) {
        let token = sessionStorage.getItem( 'userAuthToken' );
        if( token !== undefined ) {
             options.headers.append('X-CSRF-Token', token);
        }  
    }
}