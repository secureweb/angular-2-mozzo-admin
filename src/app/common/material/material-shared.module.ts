import { NgModule, ModuleWithProviders } from '@angular/core';
import { /*MdSnackBarModule, MdChipsModule, MdCardModule, MdButtonModule, MdInputModule, MdGridListModule, MdToolbarModule, MdIconModule, MdSelectModule, MdOptionModule*/ MaterialModule } from '@angular/material';

@NgModule({
  imports: [
      MaterialModule
      /*MdCardModule,
      MdButtonModule,
      MdInputModule,
      MdGridListModule,
      MdToolbarModule,
      MdIconModule,
      MdChipsModule,
      MdSnackBarModule,
      MdSelectModule,
      MdOptionModule*/
  ],
  declarations: [],
  exports: [
      MaterialModule
      /*MdCardModule,
      MdButtonModule,
      MdInputModule,
      MdGridListModule,
      MdToolbarModule,
      MdIconModule,
      MdChipsModule,
      MdSnackBarModule,
      MdSelectModule,
      MdOptionModule*/
    ]
})

export class MaterialSharedModule {
  //
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MaterialSharedModule,
      providers: []
    };
  }
}