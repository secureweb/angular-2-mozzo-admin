import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BaseService } from './base.service';
import { CoolStorageModule } from 'angular2-cool-storage';
import { MaterialSharedModule } from './material/material-shared.module';
import { KeysPipe } from './keys.pipe';
import { DialogComponent } from './dialog/dialog.component';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      CoolStorageModule,
      MaterialSharedModule
  ],
  declarations: [KeysPipe, DialogComponent],
  exports: [
      KeysPipe,
      DialogComponent,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      CoolStorageModule,
      MaterialSharedModule
    ],
    entryComponents:[DialogComponent]
})

export class SharedModule {
  //
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}