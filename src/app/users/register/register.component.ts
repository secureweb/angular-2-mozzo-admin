import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UsersModel } from '../../users/users.model';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from './password-validations';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  registerModel:any;
  formErrors = {
      'name': '',
      'email': '',
      'password': '',
      'confirmPassword': ''
    };
   
    validationMessages = {
      'name': {
        'required':      'Name is required.',
        'maxlength':     'Name cannot be more than 24 characters long.'
      },
      'email': {
        'required':      'Email is required.',
        'email':         'Email has invalid format'
      },
      'password': {
        'required':      'Password is required.',
        'minlength':     'Password must be at least 8 characters long.',
        'pattern':       'Uppercase + Special symbol + Numeric digit + lower case character'
      },
      'confirmPassword': {
        'required':      'Confirm Password is required.',
        'minlength':     'Password must be at least 8 characters long.',
        'MatchPassword': 'Password is Mis Matching'
      }
    };
  constructor (
    private formBuilder: FormBuilder,
    public snackBar: MdSnackBar,
    public authService: AuthService,
    public router: Router
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(): void {
    this.registerForm = this.formBuilder.group({
      'name': ['', [
          Validators.required,
          Validators.maxLength(24)
        ]
      ],
      'email': ['', [
        Validators.required,
        Validators.email
      ]],
      'password': ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})')
      ]],
       'confirmPassword': ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    }, {
      validator: PasswordValidation.MatchPassword // your validation method
    });

    this.registerForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.registerForm) { return; }
    const form = this.registerForm;
 
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
 
      if (control && control.dirty || !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onFocusOutFromForm(): void{
    this.onValueChanged(this.registerForm.value);
  }

  onKeyPress(): void {
    if( !this.registerForm.controls['confirmPassword'].touched ) {
      this.registerForm.controls['confirmPassword'].markAsTouched();
    }
  }

  onRegister(): void {
    this.registerModel =  new UsersModel().build(this.registerForm.value);
    this.authService.doRegister(this.registerModel).subscribe(
      authResponse => {
        if( authResponse.token ) {
          this.onRegisterSuccess(authResponse);
        } else {
          this.handleError(authResponse.error);
        }
      }, error => {
          this.handleError(error);
      }
    );
  }

  onRegisterSuccess(authResponse:any) {
      this.router.navigate(['admin']);
  }

  handleError(error) {
    this.openSnackBar(error);
  }

  openSnackBar( message:string, action?:string) {
    this.snackBar.open(message, action);
  }
}
