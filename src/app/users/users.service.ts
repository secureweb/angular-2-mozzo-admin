import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { UsersModel } from './users.model';
import { Http, Headers, Response, RequestOptions} from '@angular/http';
import { BaseService } from '../common/base.service';

@Injectable()
export class UserService extends BaseService {
  constructor(
    public http: Http
  ) { super(); }

  get(userId: number): Observable<any> {
        return this.http.get( 'admin/users/view/'+ userId )
        .map( ( response: Response ) => {
            return this.extractData(response);
        }, ( error: Response )=> {
            this.handleError(error);
        });
    }

  post( userModelPayload: UsersModel ): Observable< any > {
    return this.http.post( 'admin/users/add', userModelPayload)
      .map( ( response: Response ) => {
          return this.extractData(response);
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }

  getAll(): Observable<Array<any>> {
    return this.http.get( 'admin/users/index')
      .map( ( response: Response ) => {
          return this.extractData(response);
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }
}
