import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { UsersModel } from '../users.model';
import { UserService } from '../users.service';
import { Router } from '@angular/router';
import { AuthService } from '../../users/auth.service';

@Component({
  selector: 'app-users-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public userStatuses: Array<any> = [{ value: '0', name: 'De-Active' },{ value: '1', name: 'Active' }];
  public userForm: FormGroup;
  public userModal:UsersModel =  new UsersModel();
  public formErrors = {
      'name': '',
      'email': '',
      'password': ''
    };
   
  public validationMessages = {
      'name': {
        'required':      'Name is required.',
        'maxlength':     'Name cannot be more than 24 characters long.'
      },
      'email': {
        'required':      'Email is required.',
        'email':         'Email has invalid format'
      },
      'password': {
        'required':      'Password is required.',
        'minlength':     'Password must be at least 8 characters long.'
      }
    };
  constructor(
    private formBuilder: FormBuilder,
    public snackBar: MdSnackBar,
    public router: Router,
    public userService: UserService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(): void {
    this.userForm = this.formBuilder.group({
      'name': ['', [
          Validators.required,
          Validators.maxLength(24)
        ]
      ],
      'email': ['', [
        Validators.required,
        Validators.email
      ]],
      'password': ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    });

    this.userForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation users now
  }

  onValueChanged(data?: any) {
    if (!this.userForm) { return; }
    const form = this.userForm;
    for (const field in this.formErrors) {
      // clear previous error user (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty || !control.valid) {
        const users = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += users[key] + ' ';
        }
      }
    }
  }

  onFocusOutFromForm(): void{
    this.onValueChanged(this.userForm.value);
  }

  onSubmit(): void {
    this.userModal =  new UsersModel().build(this.userForm.value);
    this.userService.post(this.userModal).subscribe(
      userResponse => {
        if( userResponse ) {
          this.onPostSuccess(userResponse);
        } else {
          this.handleError(userResponse.error);
        }
      }, error => {
          this.handleError(error);
      }
    );
  }

  onPostSuccess(authResponse:any) {
      this.openSnackBar('User has been created successfully!!');
      setTimeout( ()=> {
        this.router.navigate(['admin/users/list']);
      }, 100);
  }

  handleError(error) {
    this.openSnackBar(error);
  }

  openSnackBar( user:string, action?:string) {
    this.snackBar.open(user, action);
  }

}
