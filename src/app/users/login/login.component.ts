import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  public loginModel:any = {
      email: "",
      password: ""
  };
  constructor(
    public authService: AuthService,
    public router: Router,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {
    
  }


  onLogin() {
    this.authService.doLogin(this.loginModel).subscribe(
      authResponse => {
        if( authResponse.token ) {
          this.onLoginSuccess(authResponse);
        } else {
          this.handleError(authResponse.error);
        }
      }, error => {
          this.handleError(error);
      }
    );
  }

  onLoginSuccess(authResponse:any) {
      this.router.navigate(['admin']);
  }

  handleError(error) {
    this.openSnackBar(error);
  }

  openSnackBar( message:string, action?:string) {
    this.snackBar.open(message, action);
  }
}
