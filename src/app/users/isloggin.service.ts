import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
@Injectable()
export class IslogginService {

  constructor(
    public authService: AuthService,
    public router: Router
  ) { }

  resolve(): void {
    if(this.authService.isLoggedIn)  this.router.navigate(['admin']);
  }
}
