import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { UsersModel } from './users.model';
import { Http, Headers, Response, RequestOptions} from '@angular/http';
import { BaseService } from '../common/base.service';
import { CoolSessionStorage } from 'angular2-cool-storage';

@Injectable()
export class AuthService extends BaseService {
  constructor(
    public http: Http,
    public sessionStorage: CoolSessionStorage
  ) { super(); }

  doLogin( loginModel:any ): Observable<any> {
      return this.http.post( 'users/login', loginModel)
      .map( ( response: Response ) => {
          response = this.extractData(response);
          this.updateUserData(response);
          return response;
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }

  doLogout() {
    return this.http.get( 'users/logout')
      .map( ( response: Response ) => {
          this.sessionStorage.clear();
          return this.extractData(response);
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }

  doRegister( userModelPayload: UsersModel ): Observable< any > {
    return this.http.post( 'users/register', userModelPayload)
      .map( ( response: Response ) => {
          response = this.extractData(response);
          this.updateUserData(response);
          return response;
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }

  private _setAuthToken(authToken) {
      this.sessionStorage.setItem( 'userAuthToken',authToken);
  }

  private _setUserDetail(userModel) {
      this.sessionStorage.setObject( 'userModel', userModel);
  }

  get authToken(): string {
    return ( this.sessionStorage.getItem( 'userAuthToken') && this.sessionStorage.getItem( 'userAuthToken').length > 0 ) ? this.sessionStorage.getItem( 'userAuthToken') : '';
  }

  get loggedInUser(): UsersModel | null {
    return ( this.sessionStorage.getItem( 'userModel') && this.sessionStorage.getItem( 'userModel').length > 0 ) ? JSON.parse( this.sessionStorage.getItem( 'userModel') ) : null;
  }

  get isLoggedIn():boolean {
    return !!this.sessionStorage.getItem( 'userAuthToken' );
  }

  public setAuthSessionStorage( response ) {
    this._setAuthToken(response.token);
    this._setUserDetail(response.user);
  }

  private updateUserData(response) {
      if( response['user'] ) {
        response['user'] =  new UsersModel().build( response['user'] );
        this.setAuthSessionStorage(response); 
      } 
  }
}
