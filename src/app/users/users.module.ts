import { NgModule } from '@angular/core';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from '../users/auth.service';
import { AuthGuard } from '../users/auth-guard.guard';
import { SharedModule } from '../common/shared.module';
import { RegisterComponent } from './register/register.component';
import { IslogginService } from './isloggin.service';
import { UserService } from './users.service';

@NgModule({
  imports: [
    SharedModule.forRoot(),
    UsersRoutingModule
  ],
  declarations: [
    UsersComponent,
    ListComponent,
    CreateComponent,
    LoginComponent,
    RegisterComponent
  ],
  providers: [
    AuthService,
    IslogginService,
    AuthGuard,
    UserService
  ],
  exports: [
    UsersComponent,
    ListComponent,
    CreateComponent
  ]
})
export class UsersModule { }
