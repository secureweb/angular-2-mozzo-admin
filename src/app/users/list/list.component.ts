import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { UserService } from '../../users/users.service';
import { UsersModel } from '../../users/users.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  users: Array< UsersModel > = [];
  constructor(
    public snackBar: MdSnackBar,
    public router: Router,
    public userservice: UserService,
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userservice.getAll().subscribe( users => {
        users.map( user => {
          this.users.push( new UsersModel().build(user) ) ;
        });
    }, error => this.handleError.bind(this, error) );
  }

  handleError(error) {
    this.openSnackBar(error);
  }
  
  openSnackBar( notification:string, action?:string) {
    this.snackBar.open(notification, action);
  }
}
