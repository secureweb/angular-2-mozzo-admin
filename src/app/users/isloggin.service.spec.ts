import { TestBed, inject } from '@angular/core/testing';

import { IslogginService } from './isloggin.service';

describe('IslogginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IslogginService]
    });
  });

  it('should be created', inject([IslogginService], (service: IslogginService) => {
    expect(service).toBeTruthy();
  }));
});
