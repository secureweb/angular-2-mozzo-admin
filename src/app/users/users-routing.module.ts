import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { IslogginService } from './isloggin.service';
import { AuthGuard } from './auth-guard.guard';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
    children: [
      { path: '', redirectTo: "login", pathMatch: 'full' },
      { path: 'login', component: LoginComponent, resolve: [IslogginService] },
      { path: 'register', component: RegisterComponent, resolve: [IslogginService] }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
