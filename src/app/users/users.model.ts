export class UsersModel {
    constructor(
        public id?:number,
        public name:string = '',
        public email:string = '',
        public status_id:string = '1',
        public created?:string,
        public modified?:string,
        public password:string = ''
    ) {}

    build( modelData : any ): any {
        for( let prop in modelData ) {
            if( this.hasOwnProperty(prop) )
            this[prop] = modelData[prop];
        }
        return this;
    }
}
