import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { MessageModel } from './messages.modal';
import { Http, Headers, Response, RequestOptions} from '@angular/http';
import { BaseService } from '../common/base.service';

@Injectable()
export class MessageService extends BaseService {
  constructor(
    public http: Http
  ) { super(); }

  get(messageId: number): Observable<any> {
        return this.http.get( 'admin/messages/view/'+ messageId )
        .map( ( response: Response ) => {
            return this.extractData(response);
        }, ( error: Response )=> {
            this.handleError(error);
        });
    }

  post( messageModelPayload: MessageModel ): Observable< any > {
    return this.http.post( 'admin/messages/add', messageModelPayload)
      .map( ( response: Response ) => {
          return this.extractData(response);
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }

  getAll(): Observable<Array<any>> {
    return this.http.get( 'admin/messages/index')
      .map( ( response: Response ) => {
          return this.extractData(response);
      }, ( error: Response )=> {
          this.handleError(error);
      });
  }
}
