import { NgModule } from '@angular/core';
import { SharedModule } from '../common/shared.module';
import { MessagesRoutingModule } from './messages-routing.module';
import { MessagesComponent } from './messages.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { MessageService } from './message.service';

@NgModule({
  imports: [
    SharedModule,
    MessagesRoutingModule
  ],
  declarations: [MessagesComponent, CreateComponent, ListComponent],
  providers:[MessageService]
})
export class MessagesModule { }
