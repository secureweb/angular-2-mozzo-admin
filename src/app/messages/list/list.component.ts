import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { MessageModel } from '../messages.modal';
import { MessageService } from '../message.service';
import { Router } from '@angular/router';
import { MdSnackBar, Sort } from '@angular/material';
import { UserService } from '../../users/users.service';
import { UsersModel } from '../../users/users.model';
import { MdDialog, MdDialogRef } from '@angular/material';
import { DialogComponent, dialogDataType } from '../../common/dialog';
import { LoaderService } from '../../common/loader/loader.service';

@Component({
  selector: 'app-messages-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  messages: Array<MessageModel> = [];
  users: Array< UsersModel > = [];
  constructor(
    public snackBar: MdSnackBar,
    public router: Router,
    public messageService: MessageService,
    public userService: UserService,
    public dialog: MdDialog,
    public loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.getMessages();
  }

  getMessages() {
    this.loaderService.showLoader();
    Observable.forkJoin([
      this.messageService.getAll().catch( error => Observable.of([])),
      this.userService.getAll().catch(error => Observable.of([]))
    ])
    .subscribe( response => {
        let messages = response[0], users = response[1];
        messages.map( message => {
          users.map( users => {
            if ( message.user_id === users.id ) {
               message.username = new UsersModel().build( users ).name;
            }
          });
          let messageModal =  new MessageModel().build(message);
          delete messageModal.user_id;
          this.messages.push( messageModal ) ;
        });
        console.log(this.messages);
    },
    error => this.handleError.bind(this, error),
    () => {
      this.loaderService.hideLoader();
    }
    );
  }

  onRowClick(message:MessageModel): void {
    this.showDialog({
        title:message.title,
        message:message.description
    });
  }

  sortData(sort: Sort) {
    const data = this.messages.slice();
    if (!sort.active || sort.direction == '') {
      this.messages = data;
      return;
    }

    this.messages = data.sort((a, b) => {
      let isAsc = sort.direction == 'asc';
      switch (sort.active) {
        case 'status_id': return this.compare(+a.status_id, +b.status_id, isAsc);
        case 'id': return this.compare(+a.id, +b.id, isAsc);
        case 'title': return this.compare(a.title, b.title, isAsc);
        default: return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


  handleError(error) {
    this.openSnackBar(error);
  }
  
  openSnackBar( message:string, action?:string) {
    this.snackBar.open( message, action );
  }

  showDialog(_dialogData: dialogDataType) {
    let dialogRef = this.dialog.open( DialogComponent, {
        data: _dialogData,
        height: '400px',
        width: '400px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
    });
  }
}
