export class MessageModel {
    constructor(
        public id?:number,
        public title:string = '',
        public description:string = '',
        public status_id:string = '0',
        public created?:string,
        public modified?:string,
        public user_id:string = '',
        public username?:string
    ) {}

    build( modelData : any ): any {
        for( let prop in modelData ) {
            if( this.hasOwnProperty(prop) )
            this[prop] = modelData[prop];
        }
        return this;
    }
}
