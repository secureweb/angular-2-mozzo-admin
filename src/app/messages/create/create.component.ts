import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { MessageModel } from '../messages.modal';
import { MessageService } from '../message.service';
import { Router } from '@angular/router';
import { AuthService } from '../../users/auth.service';

@Component({
  selector: 'app-messages-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public messageStatuses: Array<any> = [{ value: '0', name: 'De-Active' },{ value: '1', name: 'Active' }];
  public messageForm: FormGroup;
  public messageModal:MessageModel =  new MessageModel();
  public formErrors = {
      'title': '',
      'description': '',
      'status_id': ''
    };
   
  public validationMessages = {
      'title': {
        'required':      'Title is required.'
      },
      'description': {
        'required':      'Description is required.'
      },
      'status_id': {
        'required':      'Status is required.'
      }
    };
  constructor(
    private formBuilder: FormBuilder,
    public snackBar: MdSnackBar,
    public router: Router,
    public messageService: MessageService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(): void {
    this.messageForm = this.formBuilder.group({
      'title': ['', [
          Validators.required
        ]
      ],
      'description': ['', [
        Validators.required
      ]],
      'status_id': ['', [
        Validators.required
      ]]
    });

    this.messageForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.messageForm) { return; }
    const form = this.messageForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty || !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onFocusOutFromForm(): void{
    this.onValueChanged(this.messageForm.value);
  }

  onSubmit(): void {
    this.messageModal =  new MessageModel().build(this.messageForm.value);
    this.messageModal.user_id = this.authService.loggedInUser.id.toString();
    this.messageService.post(this.messageModal).subscribe(
      messageResponse => {
        if( messageResponse ) {
          this.onPostSuccess(messageResponse);
        } else {
          this.handleError(messageResponse.error);
        }
      }, error => {
          this.handleError(error);
      }
    );
  }

  onPostSuccess(authResponse:any) {
      this.openSnackBar('Post has been created successfully!!');
      setTimeout( ()=> {
        this.router.navigate(['admin/messages/list']);
      }, 100);
  }

  handleError(error) {
    this.openSnackBar(error);
  }

  openSnackBar( message:string, action?:string) {
    this.snackBar.open(message, action);
  }

}
